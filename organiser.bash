#!/usr/bin/env bash

# get/set config file values
source config/cf.cfg

# set the logging
log_filename=$(eval "date '+%Y_%m'")
log_file="${log_location}/${log_filename}-logfile.txt"
#
if [ ${logging} == "true" ] ; then
  eval mkdir -p ${log_location}
  echo "see the logfile for output: ${log_file}" &&  exec 3>&1 1>>${log_file} 2>&1
fi
echo "$(eval "date '+%Y-%m-%d %H:%M:%S'") | script is called"
# test if the host or target folder are alive
if [ ! -d "${upload_folder}" ] || [ ! -d "${target_folder}" ] ; then
  echo "$(eval "date '+%Y-%m-%d %H:%M:%S'") | upload or target folder not mounted, SHUTTING DOWN!" && echo "-----"
  exit
fi
#
# copy or move the files, this is based on the config
echo "$(eval "date '+%Y-%m-%d %H:%M:%S'") | moving files is: ${move}"
if [ ${move} == "true" ] ; then
  if [[ "$OSTYPE" == "darwin"* ]]; then
  # note: we use the -b flag here to use the 'backup' command (does not work on osx)
    transfer_command="mv -n"  # not as safe as -b! it uses the newest
  else
    transfer_command="mv -b --suffix=_dupe"  # we use this on our pi
  fi
else
  transfer_command="cp -R"
fi

# loop trough files in upload_folder
file_counter=0
for file in "${upload_folder}"/* ; do
  cur_datetime=
  start_moment=$(echo $(eval "date '+%Y-%m-%d %H:%M:%S'"))
  let "file_counter++"  #increase the file
  # get the extension and lowercase it value
  extension=$(echo ${file##*.} | tr '[:upper:]' '[:lower:]')
  # and the filename
  filename=${file##*/}

#   # if filename equals a regex we dont need to check exiftools and just mv
#   # pre defined filenames can be handled like so:
#   # 2018-02_* > will be moved to the folder: ${target_folder}/{media_save_loc}/givenName
  date_from_filename=$(echo ${filename:0:7})
  if [[ ${date_from_filename} =~ ^[0-9]{4}-[0-9]{2}$ ]]; then
    # the file matches our pattern! no need for exif!
    skip_exif="true"
  else
    skip_exif="false"
  fi

  # check what file we received
  if [[ ${extension} == "mov" ]] ; then  # file is movie file
    media_save_loc=${vid_loc}
  elif [ -d "${file}" ] ; then  # file is a folder... find out if its valid
    # check if the folder has the correct prefix
    if [ ${skip_exif} == "true" ] ; then
      eval mkdir -p "${target_folder}/${preformatted_folders_loc}" || echo "${start_moment} | error: "; ${transfer_command} "${file}" $_ || echo "${start_moment} | error: "
    else  # folder has an invalid formatting, move to _invalid
      eval mkdir -p "${target_folder}/${invalid_loc}" || echo "${start_moment} | error: "; ${transfer_command} "${file}" $_ || echo "${start_moment} | error: "
    fi
    continue
  else  # we assume it is an image file
    media_save_loc=${image_loc}
  fi

  # see if we can move the given file without exifing:
  if [ ${skip_exif} == "true" ] ; then  # pull the date from the filename
    date_folder_loc=$(echo "${date_from_filename/"-"//}")
  else
    # now we have to get the exiftags from the given file
    date_folder_loc=$(eval "exiftool -CreateDate -s3 -fast2 -d %Y/%m '${file}'")
    if [[ ${date_folder_loc} == "" ]] ; then  # exif data could not be pulled!
      suggested_prefix=$(eval "date -r  '${file}' '+%Y-%m'")  # build the suggested filename from modified date
      eval mkdir -p "${target_folder}/${no_exif_data}" || echo "${start_moment} | error: "; ${transfer_command} "${file}" "${target_folder}/${no_exif_data}/${suggested_prefix}-${filename}" || echo "${start_moment} | error: "
      continue
    else  # test if the exifdata is after the year 2000
      if [ $(echo "${date_folder_loc:0:4}") -lt 2000 ]; then
        date_folder_loc=$(echo "pre_2000${date_folder_loc:4:7}")
      fi
    fi
  fi
  # move the file
  eval mkdir -p "${target_folder}/${media_save_loc}/${date_folder_loc}" || echo "${start_moment} | error: "; ${transfer_command} "${file}" $_ || echo "${start_moment} | error: "
done
echo "$(eval "date '+%Y-%m-%d %H:%M:%S'") | run finished, total files: ${file_counter}" && echo "-----"
