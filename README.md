# Media Organiser
Takes media files (.mov for movies, all other ones get handled as photos) from a given folder and sorts them into a target directory by the mediatype and exifdata i.e.: a photo taken on november 2018 will be moved to the folder: target_folder/photos/YYYY/MM/filename.jpg (see the cfg file for the settings). When a file or folder has a name starting with YYYY-MM the mediafile gets transfered to that predefined directory.

## Usecase
I have mounted my hiDrive cloud storage via webdav to a raspberry pi. Every day at 00.00 it looks in the defined upload folder for new files to backup and sort. Our mobile phones backup to this folder. Whenever i have media files that need to be backed up i place them in this folder.

In the folder test/ there is an example of given files.
cd into the folder and run:
```
"cd media_organiser/"
```
To run the script:
```bash
"bash organiser.bash"
```

## Setup
to use the config file rename _example_cf.cfg to cf.cfg and edit it values to match your environment
```
cp config/_example_cf.cfg config/cf.cfg && open config/cf.cfg
```
The config file holds some information regarding the setup of the environment:
##### The upload and target folder
- upload_folder="test/_upload"
- target_folder="test/target"

##### logging related (if logging is set to false it will only log in the console)
- logging="true"
- log_location="test/logging"

##### Remove the originals from the upload_folder (false keeps the originals)
- move="true"

##### The target endpoint save locations based on filetype
- vid_loc="videos"
- image_loc="photos"
- preformatted_folders_loc="events"
- invalid_loc="_invalid"
- no_exif_data="_invalid/_no_exif"

### Required
Exiftools needs to be installed on the machine! https://sno.phy.queensu.ca/~phil/exiftool/install.html

### Optional
Mount your webdav folder like so (host = server, target = local path where mount should be set)
```
sudo mount -o uid=<local_user> -t davfs <host> <target>
```

### Todo
- [] Expand movie types
- [] Handle webdav mount (davfs2?)
- [] Add systemd example to run every day at time x
- [] Add a 'local' backup options to backup files locally to i.e. an external hard drive.

#### Credits
- Exiftool: https://sno.phy.queensu.ca/~phil/exiftool/
- Images (in the test folder structure) taken from: https://www.pic2map.com
- Markdown-Cheatsheet: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code
